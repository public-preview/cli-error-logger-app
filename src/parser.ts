import { processLogs } from "./services/parserService";
import { options } from "./configuration/commander";

processLogs(options);

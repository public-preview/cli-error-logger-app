import { LogLevelEnum } from "../enums/LogLevelEnum";
import { IBaseLog } from "./IBaseLog";

export interface IParsedLog extends IBaseLog {
  timestamp: number;
  loglevel: LogLevelEnum;
}

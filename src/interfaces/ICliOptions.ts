export interface ICliOptions {
  input: string;
  output: string;
}

export interface IBaseLog {
  transactionId: string;
  err: string;
}

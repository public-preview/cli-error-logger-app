import { IBaseLog } from "./IBaseLog";

export interface IErrorLogBody extends IBaseLog {
  code: string;
  details: string;
}

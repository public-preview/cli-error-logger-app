import fs from "fs";
import { ICliOptions } from "../interfaces/ICliOptions";
import { LogLevelEnum } from "../enums/LogLevelEnum";
import { IParsedLog } from "../interfaces/IParsedLog";
import { IErrorLogBody } from "../interfaces/IErrorLogBody";
import { isFileExist } from "../utils/filesUtil";
import logger from "../configuration/logger";

export const processLogs = async ({
  input,
  output,
}: ICliOptions): Promise<void> => {
  if (!isFileExist(input)) {
    throw new Error(
      `File "${input}" does not exist. Check path and run it again.`
    );
  }

  const readLogsStream = fs.createReadStream(input, "utf-8");
  const writeParsedStream = fs.createWriteStream(output, "utf-8");

  let isEmpty = true;
  let counter = 0;
  writeParsedStream.write("[");
  readLogsStream.on("data", (chunk: string) => {
    logger.info("Start parsing.");
    const arrOfLogs = chunk.split("\n");
    for (let log of arrOfLogs) {
      if (log.includes(`- ${LogLevelEnum.ERROR} -`)) {
        if (!isEmpty) {
          writeParsedStream.write(",");
        }
        const parsedLog: IParsedLog = parseLog(log);
        writeParsedStream.write(JSON.stringify(parsedLog));

        isEmpty = false;
        counter++;
      }
    }
  });

  readLogsStream.on("end", () => {
    writeParsedStream.write("]");
    writeParsedStream.close();

    logger.info(`${counter} Error logs were parsed.`);
    logger.info("Parsing is finished.");
  });
};

export const parseLog = (log: string): IParsedLog => {
  try {
    const timestamp = parseDate(log);
    const loglevel = parseLogLevel(log);
    const { transactionId, err } = parseBody(log);

    return {
      timestamp,
      loglevel,
      transactionId,
      err: err ? err : "",
    };
  } catch (error) {
    logger.error(`Unable to parse log: ${log}.`);
    throw error;
  }
};

export const parseDate = (log: string): number => {
  const datePattern =
    /^\d{4}-\d{1,2}-\d{1,2}T\d{1,2}:\d{1,2}:\d{1,2}.\d{1,3}Z/g;
  const result: RegExpExecArray | null = datePattern.exec(log);

  if (!result?.length) {
    throw new Error("Date couldn't be parsed.");
  }

  return new Date(result[0]).getTime();
};

export const parseLogLevel = (log: string): LogLevelEnum => {
  const levelPattern = /- (.*?) -/g;
  const result: RegExpExecArray | null = levelPattern.exec(log);

  if (!result?.length) {
    throw new Error("Log Level couldn't be parsed.");
  }

  return result[0].replace(/-/g, "").trim() as LogLevelEnum;
};

export const parseBody = (log: string): IErrorLogBody => {
  const bodyPattern = /{(.*)}/g;
  const result: RegExpExecArray | null = bodyPattern.exec(log);

  if (!result?.length) {
    throw new Error("Log Body couldn't be parsed.");
  }

  try {
    return JSON.parse(result[0]) as IErrorLogBody;
  } catch (e) {
    throw new Error("Unable to parse log body json.");
  }
};

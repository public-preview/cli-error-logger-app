import fs from "fs";

/**
 * Check if file exist
 *
 * @param {string} path
 * @returns {boolean}
 */
export const isFileExist = (path: string) => {
  return fs.existsSync(path);
};

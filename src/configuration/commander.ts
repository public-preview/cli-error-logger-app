import { Command } from "commander";
import { textSync } from "figlet";
import { ICliOptions } from "../interfaces/ICliOptions";

console.log(textSync("Log parser app"));
console.log("\n\n");

const commander = new Command();
commander
  .version("1.0.0")
  .description("Log parser app")
  .requiredOption("-i, --input <value>", "Input path of to log file.")
  .requiredOption("-o, --output <value>", "Output path for parsed json file.")
  .parse(process.argv);

export const options: ICliOptions = commander.opts();

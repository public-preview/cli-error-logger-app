import log4js from "log4js";

log4js.configure({
  appenders: { local: { type: "console" } },
  categories: { default: { appenders: ["local"], level: "info" } },
});

const logger = log4js.getLogger("[Log Parser]");

export default logger;

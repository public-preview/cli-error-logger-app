export enum LogLevelEnum {
  ERROR = "error",
  INFO = "info",
  DEBUG = "debug",
  WARN = "warn",
}

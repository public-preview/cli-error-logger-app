import { describe } from "node:test";
import logger from "../src/configuration/logger";

describe("Logger Util", () => {
  test("logger work properly", () => {
    logger.log = jest.fn();
    logger.log("super parser");
    expect(logger.log).toHaveBeenCalledWith("super parser");
  });
});

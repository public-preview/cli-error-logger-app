import { isFileExist } from "../src/utils/filesUtil";

describe("Files Util", () => {
  test("isFileExist work properly", () => {
    expect(isFileExist("./app.log")).toBe(true);
    expect(isFileExist("./app2.log")).toBe(false);
  });
});

import {
  processLogs,
  parseDate,
  parseLogLevel,
  parseBody,
  parseLog,
} from "../src/services/parserService";

const logRow = `2021-08-09T02:12:51.259Z - error - {"transactionId":"9abc55b2-807b-4361-9dbe-aa88b1b2e978","details":"Cannot find user orders list","code": 404,"err":"Not found"}`;
const brokenLogRow = `20210809T02:12:51.259Z - error- {""transactionId":"9abc55b2-807b-4361-9dbe-aa88b1b2e978","details":"Cannot find user orders list","code": 404,"err":"Not found"}`;

describe("Parser Service", () => {
  test("processLogs work properly", () => {
    expect(
      processLogs({ input: "./app.log", output: "./errors.json" })
    ).resolves.not.toThrowError();

    expect(
      processLogs({ input: "./app2.log", output: "./errors.json" })
    ).rejects.toThrowError();
  });

  test("parseLog work properly", () => {
    expect(parseLog(logRow)).toMatchObject({
      timestamp: 1628475171259,
      loglevel: "error",
      transactionId: "9abc55b2-807b-4361-9dbe-aa88b1b2e978",
      err: "Not found",
    });
  });

  test("date parsed properly", () => {
    expect(parseDate(logRow)).toBe(1628475171259);
    try {
      parseDate(brokenLogRow);
    } catch (e) {
      expect(e.message).toBe("Unable to parse date.");
    }
  });

  test("logLever parsed properly", () => {
    expect(parseLogLevel(logRow)).toBe("error");
    try {
      parseLogLevel(brokenLogRow);
    } catch (e) {
      expect(e.message).toBe("Unable to parse log level.");
    }
  });

  test("log body parsed properly", () => {
    expect(parseBody(logRow)).toMatchObject({
      code: 404,
      details: "Cannot find user orders list",
      transactionId: "9abc55b2-807b-4361-9dbe-aa88b1b2e978",
      err: "Not found",
    });

    try {
      parseBody(brokenLogRow);
    } catch (e) {
      expect(e.message).toBe("Unable to parse log body json.");
    }
  });
});

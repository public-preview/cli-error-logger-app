# Error log parser CLI App

## Run dev

`npm run start:dev`

## Build

`tsc`

## Run Prod(compiled version)

`npm run start:prod`

## Run tests

`npm test`
